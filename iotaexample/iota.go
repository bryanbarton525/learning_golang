package iotaexample

import "fmt"

const (
	a = iota
	b
	c
)

const (
	d = iota
	e
	f
)

// UseIota is a function showing and example of constants and iota
func UseIota() {

	fmt.Println("--------------------------------")
	fmt.Println("*** iota Example ***")
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println("Iota resets since new constant was defined starting with d")
	fmt.Println(d)
	fmt.Println(e)
	fmt.Println(f)
	fmt.Println("--------------------------------")
}
