package forloop

import "fmt"

// ForExample1 is an example of the init, condition, post method
func ForExample1() {
	for i := 0; i <= 10; i++ {
		fmt.Println(i)
	}
	fmt.Println("Example 1 Done")
}

// ForExample2 is an example of the single condition method
func ForExample2() {
	x := 0
	for x < 10 {
		fmt.Println(x)
		x++
	}
	fmt.Println("Example 2 Done")
}
