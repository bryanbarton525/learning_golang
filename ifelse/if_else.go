package ifelse

import "fmt"

// ExampleIfElse prints out a string depending on whether the variables match
func ExampleIfElse() {
	fmt.Println("--------------------------------")
	fmt.Println("***IF Else Example***")
	x := 42
	y := 43

	fmt.Printf("The Variable x's value is %v\n", x)
	fmt.Printf("The Variable y's value is %v\n", y)

	if x == y {
		fmt.Println("Yay they match")
	} else {
		fmt.Println("Oh they dont match")
		z := x == y
		fmt.Println("Result of statement z := x == y")
		fmt.Println(z)
	}
	fmt.Println("--------------------------------")
}
