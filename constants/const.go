package constants

import "fmt"

// two different ways to define constants
const a = 42

const (
	b = 42.65
	c = "We are having fun"
	d = true
)

// ExampleConstant prints out the types and values of the constant variables assigned
func ExampleConstant() {
	fmt.Println("-----------------------------")
	fmt.Println("**Constants Example**")

	fmt.Printf("Variable a's type is %T Variable a equals %v\n", a, a)

	fmt.Printf("Variable b's type is %T Variable b equals %v\n", b, b)

	fmt.Printf("Variable c's type is %T Variable c equals %v\n", c, c)

	fmt.Printf("Variable d's type is %T Variable d equals %v\n", d, d)
	fmt.Println("-----------------------------")
}
