# Package Layout
```
learn_golang
│   README.md
│   main.go    
│
└───contstants
│   │   const.go
│   
└───forloop
|   │   forloop.go
|
└───ifelse
│   │   if_else.go
│   
└───systemspec
|   │   runtime.go
│   
└───iotaexample
    │   iota.go
```
### It's important to make sure, in the directory tree and package structure, that the package name in your code matches the directory name. For example if the directory name is foo then the code would look like this:
```
package foo

import "fmt"

// Bar is an example function
func Bar() {
    fmt.Println("This is an example")
}
```
### Also, if you want to make a variable or function publicly available to other packages then the first letter MUST be CAPITALIZED. As shown in the above example.
