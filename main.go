package main

import (
	"fmt"
	"practice/constants"
	"practice/forloop"
	"practice/ifelse"
	"practice/iotaexample"
	"practice/systemspec"
	"practice/userinput"
	"strings"
	"time"
)

func main() {
	// Displays Main Menu
	for {
		state := menu()
		if state == false {
			break
		}
	}

}

// Menu function used to produce menu items, wait for user input, and map user input to a func
func menu() bool {
	var option int
	var proceed string

	menuItems := map[int]func(){
		1: constants.ExampleConstant,
		2: ifelse.ExampleIfElse,
		3: systemspec.MySystem,
		4: iotaexample.UseIota,
		5: forloop.ForExample1,
		6: forloop.ForExample2,
		7: userinput.InputExample1,
	}

	// Print Menu options for user input
	fmt.Println("***Code Example Go App***")
	fmt.Println(`
	1) Constants Example
	2) If Else Example
	3) Runtime Example
	4) Iota Example
	5) For Loop Init, Condition, Post
	6) For Loop Single Condition
	7) User Input fmt Example
	0) Exit
	`)

	fmt.Println("Select an option:")

	// scan user input
	if _, err := fmt.Scanln(&option); err != nil {
		strings.Contains(err.Error(), "expected integer")
		fmt.Println("Please select an option from the list above...")
		time.Sleep(2 * time.Second)
		return true

	} else if option >= 1 {
		fmt.Printf("Your selection was %v\n", option)
		menuItems[option]()
		fmt.Println("Press Enter to Continue...")
		fmt.Scanln(&proceed)
		return true

	} else {
		fmt.Println("Your selection was....")
		fmt.Println(option)
		return false
	}

}
