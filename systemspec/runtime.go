package systemspec

import (
	"fmt"
	"runtime"
)

// MySystem prints out the runtime os and architecture
func MySystem() {
	fmt.Println("--------------------------------")
	fmt.Println("*** Runtime Example ***")
	fmt.Printf("OS: %v\n", runtime.GOOS)
	fmt.Printf("Processor Type: %v\n", runtime.GOARCH)
	fmt.Println("--------------------------------")
}
