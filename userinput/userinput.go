package userinput

import (
	"bufio"
	"fmt"
	"os"
)

// InputExample1 is a function showing how to collect user input using the fmt package
func InputExample1() {
	fmt.Println("-------------------------------")
	fmt.Println("*** User Input using FMT Example ***")
	fmt.Println(`
	// InputExample1 is a function showing how to collect user input using the fmt package
	func InputExample1() {
		var age int
		fmt.Println("What is your age?")
		fmt.Scanln(&age)
		fmt.Println("Your age is:")
		fmt.Println(age)
	}
	`)
	var age int
	fmt.Println("What is your age?")
	fmt.Scanln(&age)
	fmt.Printf("Your age is: %d\n", age)
	fmt.Println("-------------------------------")
}

// InputExample2 is a function showing how to collect user input using the bufio package
func InputExample2() {
	//reading a string
	reader := bufio.NewReader(os.Stdin)
	var name string
	fmt.Println("What is your name?")
	name, _ = reader.ReadString('\n')

	fmt.Println("Your name is ", name)
}
